= Bug Reporting Guide =

One of the most important contribution task is reporting the bugs you have found.

All bugs and suggestions should be reported to one of our [wiki:QubesLists mailing list]. We decided that at this stage access to the wiki and the [report:3 ticketing system] should be limited and per-invitation only, and generally granted to those people who actually can first demonstrate that they are capable of contributing something useful to the project. Our main goal is to build a quality product, and this has a priority over building "a large community".

So if you have not allowed to file new tickets You should follow these guide lines:

 1. Search the issue tracker to see if your issue is already there - if so please cite the issue # in any comments you post.
   * [search: Simple search]
   * [https://wiki.qubes-os.org/query?status=accepted&status=assigned&status=new&status=reopened&order=priority Advanced search]

 2. Search the [wiki:QubesLists mailing lists] to see if your issue is already discussed - if so, add your comments to the existing thread.

 3. If your problem seems to be a new one, report it:
   * as a mail, with a clear specific subject like "bug report: <specific description>"
   * include steps to reproduce the problem if possible, and relevant log files (if you know)
   * if its a hardware related issue include your [wiki:HCL] Support files as well.

 5. Qubes [wiki:QubesDevelopers developers] may ask you additional questions - please do follow up.  

 6. If the issue can be validated as a definite bug then they enters it into the [report:3 issue tracker].