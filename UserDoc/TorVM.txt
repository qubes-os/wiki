{{{
#!Markdown
Qubes TorVM (qubes-tor)
==========================

Qubes TorVM is a ProxyVM service that provides Torified networking to all its
clients.

By default, any AppVM using the TorVM as its NetVM will be fully Torified.
(Such AppVMs are referred to as "AnonVMs.")
This means that even applications which are not "Tor aware" will be unable to access the outside network directly.
Moreover, AnonVMs are not able to access globally identifying information (IP address and MAC address).

Due to the nature of the Tor network, only IPv4 TCP and DNS traffic are allowed.
All non-DNS UDP and IPv6 traffic is silently dropped.

See [this article](http://theinvisiblethings.blogspot.com/2011/09/playing-with-qubes-networking-for-fun.html) for a description of the concept, architecture, and the original implementation.

## Warning + Disclaimer

1. Qubes TorVM is produced independently from the Tor(R) anonymity software and
   carries no guarantee from The Tor Project about quality, suitability or
   anything else.

2. Qubes TorVM is not a magic anonymizing solution. Protecting your identity
   requires a change in behavior. Read the "Protecting Anonymity" section
   below.

3. Traffic originating from the TorVM itself **IS NOT** routed through Tor.
   This includes system updates to the TorVM. Only traffic from VMs using TorVM
   as their NetVM is torified.

Installation
============


0. *(Optional)* If you want to use a separate vm template for your TorVM

        qvm-clone fedora-20-x64 fedora-20-x64-net

1. In dom0, create a proxy vm and disable unnecessary services and enable qubes-tor

        qvm-create -p torvm
        qvm-service torvm -d qubes-netwatcher
        qvm-service torvm -d qubes-firewall
        qvm-service torvm -e qubes-tor
          
        # if you  created a new template in the previous step
        qvm-prefs torvm -s template fedora-20-x64-net

2. From your template vm, install the torproject Fedora repo

        sudo yum install qubes-tor-repo

3. Then, in the template, install the TorVM init scripts

        sudo yum install qubes-tor

4. Configure an AppVM to use TorVM as its netvm (example a vm named anon-web)

        qvm-prefs -s anon-web netvm torvm
	... repeat for other appvms ...

5. Modify Qubes policies to prevent accidental clearnet leaks from the new AnonVM

        /etc/qubes-rpc/policy/qubes.OpenInVM
        
        anon-web  $dispvm  deny
        $anyvm    $dispvm  allow
        $anyvm    $anyvm   ask

        /etc/qubes-rpc/policy/qubes.VMShell

        anon-web  $dispvm  deny
        $anyvm    $dispvm  allow
        $anyvm    $anyvm   deny   # Recommended global modification

    (See [here](https://groups.google.com/d/msg/qubes-users/xnAByaL_bjI/3PjYdiTDW-0J) for an explanation of the recommended global modification in the last line.)

6. Shutdown templateVM.

7. Set prefs of torvm to use your default netvm or firewallvm as its NetVM

8. Start the TorVM and any AppVM you have configured to use it

9. From the AppVM, verify torified connectivity

        curl https://check.torproject.org

### Troubleshooting ###

1. Check if the qubes-tor service is running (on the torvm)

        [user@torvm] $ sudo service qubes-tor status

2. Tor logs to syslog, so to view messages use

        [user@torvm] $ sudo grep Tor /var/log/messages

3. Restart the qubes-tor service (and repeat 1-2)

        [user@torvm] $ sudo service qubes-tor restart

Usage
=====

Applications should "just work" behind a TorVM, however there are some steps
you can take to protect anonymity and increase performance.

## Protecting Anonymity

The TorVM only purports to prevent the leaking of two identifiers:

1. WAN IP Address
2. NIC MAC Address

This is accomplished through transparent TCP and transparent DNS proxying by
the TorVM.

The TorVM cannot anonymize information stored or transmitted from your AppVMs
behind the TorVM. 

*Non-comprehensive* list of identifiers TorVM does not protect:

* Time zone
* User names and real name
* Name+version of any client (e.g. IRC leaks name+version through CTCP)
* Metadata in files (e.g., exif data in images, author name in PDFs)
* License keys of non-free software

In particular, using a TorVM does nothing by itself to combat [browser fingerprinting](https://www.torproject.org/projects/torbrowser/design/#fingerprinting-linkability). For this, it is recommended that you use [Tor Browser](https://www.torproject.org/download/download-easy.html.en) with "Transparent Torification" selected in it Proxy Settings in order to avoid running Tor over Tor (which is strongly discouraged and potentially dangerous). Likewise, if you use Thunderbird in an AnonVM, it is recommended that you use the [TorBirdy](https://trac.torproject.org/projects/tor/wiki/torbirdy) extension in order to prevent Thunderbird from leaking potentially identifying information about you.

By default, DispVMs in Qubes use the firewallvm as their NetVM. This can be dangerous if, for example, you download a file in an AnonVM, then open it in a DispVM, and it "phones home" over your clearnet connection from the DispVM. For this reason, you may wish to [customize your DispVM template](wiki:UserDoc/DispVMCustomization) in order to change the default NetVM to "none." Then, whenever you start a new DispVM, you can manually select the desired ProxyVM (e.g., firewallvm, torvm) or simply leave it network-disconnected. This also prevents any default applications from automatically collecting and/or divulging identifying information (e.g., via an automated update-check process) over your clearnet connection before the NetVM is changed (which might be the case if a default firewallvm-connected DispVM is started, then switched to connect to the TorVM).

### Further Reading

* Tor
    * [Official Tor usage warning](https://www.torproject.org/download/download-easy.html.en#warning)
    * [Information on protocol leaks](https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO#Protocolleaks)
* Tor Browser
    * [Tor Browser design](https://www.torproject.org/projects/torbrowser/design/)
    * [How to use Tor Browser with TorVM](https://groups.google.com/d/topic/qubes-devel/Dd0MVbIam5I/discussion)
    * [Why should I use Tor Browser with TorVM?](https://groups.google.com/d/topic/qubes-devel/6ewqvvwj7I4/discussion)
* Qubes TorVM
    * [TorVM implementation and general usage](https://groups.google.com/d/topic/qubes-devel/C1D5_tbl4jM/discussion)
    * Is there any reason to use multiple TorVMs?
        * [On stream isolation](https://groups.google.com/d/topic/qubes-devel/le7-Rrq6yxY/discussion)
        * [On zero-day resiliency](https://groups.google.com/d/topic/qubes-users/xWAO3WpXn9k/discussion)
    * Comparison to similar software
        * [Comparing TorVM to Whonix](https://groups.google.com/d/topic/qubes-devel/GT8LyE-la-o/discussion)
        * [Whonix, Tails, Tor Browser, and Qubes TorVM compared at the Whonix wiki](https://www.whonix.org/wiki/Comparison_with_Others)

## Performance

In order to mitigate identity correlation TorVM makes heavy use of Tor's new
[stream isolation feature][stream-isolation]. Read "Threat Model" below for more information.

However, this isn't desirable in all situations, particularly web browsing.
These days loading a single web page requires fetching resources (images,
javascript, css) from a dozen or more remote sources. Moreover, the use of
IsolateDestAddr in a modern web browser may create very uncommon HTTP behavior
patterns, that could ease fingerprinting.

Additionally, you might have some apps that you want to ensure always share a
Tor circuit or always get their own.

For these reasons TorVM ships with two open SOCKS5 ports that provide Tor
access with different stream isolation settings:

* Port 9049 - Isolates destination port and address, and by SOCKS Auth  
	      Same as default settings listed above, but each app using a unique SOCKS
              user/pass gets its own circuit.
* Port 9050 - Isolates by SOCKS Auth and client address only  
              Each AppVM gets its own circuit, and each app using a unique SOCKS
              user/pass gets its own circuit

SOCKS Port 9050 should be used by web browsers.

## Custom Tor Configuration

Default tor settings are found in the following file and are the same across
all TorVMs.

      /usr/lib/qubes-tor/torrc

You can override these settings in your TorVM, or provide your own custom
settings by appending them to:

      /rw/usrlocal/etc/qubes-tor/torrc

For information on tor configuration settings `man tor`

## Enforcing Firewall Rules in AnonVMs

Tor works by wrapping traffic in multiple layers of encryption. Therefore, any traffic which passes from an AnonVM through a TorVM is completely encrypted. Since this traffic is encrypted, [Qubes firewall](wiki:QubesFirewall) rules cannot be applied to it in the usual way. If you wish to enforce firewall rules in an AnonVM, you must create another FirewallVM and place it in between your AnonVM and your TorVM, like so:

        [anonvm]---[firewallvm1]---[torvm]---[firewallvm2]---[netvm]

(In this image, "firewallvm2" is the Qubes default FirewallVM, while "firewallvm1" is the newly-created FirewallVM.)

The desired firewall rules should then be created on the "Firewall rules" tab of the AnonVM's configuration page. The FirewallVMs themselves do not require any particular firewall settings and will function correctly with maximally restrictive settings (i.e., "deny all").


Threat Model
============

TorVM assumes the same Adversary Model as [TorBrowser][tor-threats], but does
not, by itself, have the same security and privacy requirements.

## Proxy Obedience

The primary security requirement of TorVM is *Proxy Obedience*.

Client AppVMs MUST NOT bypass the Tor network and access the local physical
network, internal Qubes network, or the external physical network.

Proxy Obedience is assured through the following:

1. All TCP traffic from client VMs is routed through Tor
2. All DNS traffic from client VMs is routed through Tor
3. All non-DNS UDP traffic from client VMs is dropped
4. Reliance on the [Qubes OS network model][qubes-net] to enforce isolation

## Mitigate Identity Correlation

TorVM SHOULD prevent identity correlation among network services.

Without stream isolation, all traffic from different activities or "identities"
in different applications (e.g., web browser, IRC, email) end up being routed
through the same tor circuit. An adversary could correlate this activity to a
single pseudonym.

By default TorVM uses the most paranoid stream isolation settings for
transparently torified traffic:

* Each AppVM will use a separate tor circuit (`IsolateClientAddr`)
* Each destination port will use a separate circuit (`IsolateDestPort`)
* Each destination address will use a separate circuit (`IsolateDestAdr`)

For performance reasons less strict alternatives are provided, but must be
explicitly configured.

Future Work
===========
* Integrate Vidalia
* Create Tor Browser packages w/out bundled tor
* Use local DNS cache to speedup queries (pdnsd)
* Support arbitrary [DNS queries][dns]
* Fix Tor's openssl complaint
* Support custom firewall rules (to support running a relay)
* Integrate Whonix into Qubes ([discussion](https://groups.google.com/d/topic/qubes-devel/2vnGqsoM9p0/discussion))

Acknowledgements
================

Qubes TorVM is inspired by much of the previous work done in this area of
transparent torified solutions. Notably the following:

* [adrelanos](mailto:adrelanos@riseup.net) for his work on [aos/Whonix](https://sourceforge.net/p/whonix/wiki/Security/)
* The [Tor Project wiki](https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO)
* And the many people who contributed to discussions on [tor-talk](https://lists.torproject.org/pipermail/tor-talk/)

[stream-isolation]: https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/171-separate-streams.txt
[tor-threats]: https://www.torproject.org/projects/torbrowser/design/#adversary
[qubes-net]: [wiki:QubesNet]
[dns]: https://tails.boum.org/todo/support_arbitrary_dns_queries/
}}}

== Known issues: ==
 * [https://groups.google.com/d/msg/qubes-users/fyBVmxIpbSs/R5mxUcIEZAQJ Service doesn't start without (even empty) user torrc]

Source of this document: http://git.qubes-os.org/gitweb/?p=marmarek/qubes-app-linux-tor.git;a=blob;f=README.md